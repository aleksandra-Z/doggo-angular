(function(){
    
    "use strict";
    
    angular
    .module("doggoApp")
    .factory("doggoFactory", function($http){
       
        function getDogList(){
            return $http.get('https://dog.ceo/api/breeds/list/all');
        }
        
        return {
          getDogList: getDogList 
        } 
    })
    .factory("doggoPicFactory", function($http){
       
        function getPic(breed){
            return $http.get('https://dog.ceo/api/breed/'+breed+'/images/random');
        }
        
        return {
          getPic: getPic 
        } 
    });
    
})();