(function(){
    
    "use strict";
    
    angular
    .module("doggoApp")
    .controller("doggoAppCtrl", function($scope, $http, doggoFactory, doggoPicFactory, $mdToast){
        
        doggoFactory.getDogList().then(function(dogs){
           $scope.breeds = Object.keys(dogs.data.message);   
        });
        
        
        $scope.changeBreed = function(breed){
            $scope.isDogVisible = true;
            doggoPicFactory.getPic(breed)
                .then(function(response) {
                $scope.picJsonLink = response.data.message;
                
            });
            
            $mdToast.show(
                    $mdToast.simple()
                    .content("Please wait until the image gets loaded")
                    .hideDelay(1500)
                    .position("top, right")
            );
            
        }
        
        $scope.openDropdownList = function(){
            $scope.isDropdownVisible = $scope.isDropdownVisible ? false : true;
        }
        
        
         
    });
})();